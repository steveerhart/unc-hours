class AddLine3ToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :line3, :string
  end
end
