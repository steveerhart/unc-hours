class AddDefaultFieldsToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :alternate_name, :string
    add_column :locations, :url, :string
    add_column :locations, :calendar, :text
    add_column :locations, :phone, :string
    add_column :locations, :handicap_accessible, :boolean
    add_column :locations, :description, :text
    
    
  end
end
