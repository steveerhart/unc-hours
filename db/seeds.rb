# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

locations = YAML.load_file 'db/location_seeds.yml'

locations.each do |l|
  @l = Location.new
  @a = Address.new
  
  %w{line1 line2 line3 city state zip}.each do |a|
    @a[a] = l['address'][a]
  end
  
  %w{name alternate_name url description phone handicap_accessible lat lon}.each do |a|
    @l[a] = l[a]
  end
  
  @l.address = @a

  @l.save!
  
end
