class Location < ActiveRecord::Base
  has_one :address, as: :addressable
  accepts_nested_attributes_for :address
  
  def to_param
    "#{name}".parameterize
  end

end
