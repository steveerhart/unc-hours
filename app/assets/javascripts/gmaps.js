var gmaps = (function(){

    var map,
        mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(49.26565525013921, -123.25337290763855),
            disableDefaultUI: true,
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT,
                style: google.maps.ZoomControlStyle.SMALL
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


    return {

        initializeMap: function(){

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            // height of the map is based of the locations table height
            // minus the 30 for the tables header
            $('#map-canvas').css('height', $('#locations').height() - 40);
            $('#content').css('height', $('#map-canvas').height() - 30);
        }
    };

})();
