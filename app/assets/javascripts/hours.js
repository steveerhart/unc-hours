$(function(){
        
    var $map = $('#map'),
        $content = $('#content');

    $('tbody .location')
        .on('click', function(e){
            $map.removeClass('poke');
            $map.addClass('hidden');
        })
        .hover(
            function(e){
                if( ! $map.hasClass('hidden') ) {
                    $map.addClass('poke');
                }
            },
            function(e){
                $map.removeClass('poke');
            });
               
    $('#slide-in').on('click', function(e){
        $map.removeClass('hidden');
    });

    google.maps.event.addDomListener(window, 'load', gmaps.initializeMap());

});
