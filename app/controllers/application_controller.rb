class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def index
    
    @date_time = DateTime.now
    
    respond_to do |format|

      format.html { @locations = Location.all }

      format.js  { render partial: 'location_show', locals: { location: Location.find(params[:id]) } }
      
    end
  end
  
  
end
